<?php

namespace app\checks;

abstract class Check
{
    protected $data;
    protected $params;

    public function __construct(array $data)
    {
        $this->data = $data;
        $this->params = [];
    }

    abstract public function makeCheck();

    public function getParams()
    {
        return $this->params;
    }

}