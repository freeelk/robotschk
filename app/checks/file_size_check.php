<?php

namespace app\checks;

class File_Size_Check extends check
{
    const MAX_FILE_SIZE = 32768;

    public function makeCheck()
    {

        if (!isset($this->data['requestResponse']['fileSize'])) {
            throw new \Exception('fileSize is not exists in source data');
        }

        if ($this->data['requestResponse']['httpCode'] == '404') {
            throw new \Exception('file is not exists (httpcode 404)');
        }


        $this->params[] = $this->data['requestResponse']['fileSize'].' байт';

        return ($this->data['requestResponse']['fileSize']
            <= Self::MAX_FILE_SIZE);
    }

}