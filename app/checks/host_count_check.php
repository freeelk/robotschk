<?php

namespace app\checks;

class Host_Count_Check extends check
{

    public function makeCheck()
    {

        if (!isset($this->data['requestResponse']['content'])) {
            throw new \Exception(
                'content of robots.txt is not exists in source data'
            );
        }

        $robotsParserHelper = new \app\helpers\Robots_Parser_Helper(
            $this->data['requestResponse']['content']
        );

        $values = $robotsParserHelper->getValues('Host');
        $this->params[] = count($values);

        if (count($values) == 0) {
            throw new \Exception('Host directive is not exist in robots.txt');
        }

        return (count($values) == 1);

    }

}