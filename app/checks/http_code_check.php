<?php

namespace app\checks;

class Http_Code_Check extends check
{

    public function makeCheck()
    {

        if (!isset($this->data['requestResponse']['httpCode'])) {
            throw new \Exception('httpCode is not exist in source data');
        }

        $this->params[] = $this->data['requestResponse']['httpCode'];

        return ($this->data['requestResponse']['httpCode'] == '200');
    }

}