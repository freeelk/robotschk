<?php

namespace app\checks;

class Robots_Exists_Check extends check
{

    public function makeCheck()
    {
        if ($this->data['requestResponse']['httpCode'] == '404') {
            return false;
        }

        return isset($this->data['requestResponse']['content']);
    }

}