<?php

namespace app\checks;

class Sitemap_Exists_Check extends check
{

    public function makeCheck()
    {

        if (!isset($this->data['requestResponse']['content'])) {
            throw new \Exception(
                'content of robots.txt is not exist in source data'
            );
        }

        $robotsParserHelper = new \app\helpers\Robots_Parser_Helper(
            $this->data['requestResponse']['content']
        );

        $values = $robotsParserHelper->getValues('Sitemap');

        return (count($values) != 0);

    }

}