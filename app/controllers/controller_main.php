<?php namespace app\controllers;

class Controller_Main extends Controller
{
    function action_index()
    {
        $data['useragent'] = $_SERVER ['HTTP_USER_AGENT'];

        $this->view->generate('main_view.php', 'template_view.php', $data);
    }

}