<?php namespace app\controllers;

class Controller_Result extends Controller
{
    function action_index()
    {
        $data['url'] = $_POST['url'];
        $data['redirect-enable'] = isset($_POST['redirect-enable']);
        $data['useragent-enable'] = isset($_POST['useragent-enable']);

        if (trim($_POST['useragent']) == '') {
            $data['useragent'] = $_SERVER ['HTTP_USER_AGENT'];
        } else {
            $data['useragent'] = $_POST['useragent'];
        }

        $robotRequestHelper = new \app\helpers\Robots_Request_Helper($data['redirect-enable'],
            ($data['useragent-enable'] ? $data['useragent'] : '' ) );
        $data['requestResponse'] = $robotRequestHelper->makeRequest(
            $_POST['url']
        );

        $models = \app\models\table_models\Checks_Table_Model::find();

        $modelChecks = new \app\models\Model_Checks($models, $data);

        $checksResult = $modelChecks->makeChecks();
        $data['checksResult'] = $checksResult;

        $this->view->generate('result_view.php', 'template_view.php', $data);
    }

}