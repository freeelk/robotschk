<?php namespace app\core;

final class Database
{
    const CONNECTION_INI_FILE = 'connection_params.ini';

    private static $instance;
    private $connection;

    private function __construct()
    {
        $connParameters = $this->getConnParameters();

        $connStr = 'mysql:host='.$connParameters['mysql_host'].';dbname='
            .$connParameters['mysql_dbname'];
        try {
            $this->connection = new \PDO(
                $connStr,
                $connParameters['mysql_user'],
                $connParameters['mysql_password']
            );
            $this->connection->exec("set names utf8");
            $this->connection->setAttribute(
                \PDO::ATTR_ERRMODE,
                \PDO::ERRMODE_EXCEPTION
            );
        } catch (\PDOException $Exception) {
            exit("Cannot connect to database. Please check connection parameters");
        }
    }

    private function getConnParameters()
    {
        return $connParameters = $GLOBALS['connectionParameters'];
    }

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getConnection()
    {
        return $this->connection;
    }

    private function __clone()
    {
    }
}
