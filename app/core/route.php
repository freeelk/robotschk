<?php namespace app\core;

class Route
{

    static function start()
    {

        $controller_name = 'main';
        $action_name = 'index';
        $data = [];

        $routes = explode('/', $_SERVER['REQUEST_URI']);

        if (!empty($routes[1])) {
            $controller_name = $routes[1];
        }

        if (!empty($routes[2])) {
            $action_name = $routes[2];
        }

        if (!empty($routes[3])) {
            $data['format'] = $routes[3];
        }

        $controller_name = '\app\controllers\Controller_'.$controller_name;
        $action_name = 'action_'.$action_name;

        try {
            \class_exists($controller_name);
        } catch (\Exception $E) {
            Route::ErrorPage404();
        }

        $controller = new $controller_name;
        $action = $action_name;

        if (method_exists($controller, $action)) {
            $controller->$action($data);
        } else {
            Route::ErrorPage404();
        }

    }

    function ErrorPage404()
    {
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:'.$host.'404');
    }

}