<?php
namespace app\helpers;

class Robots_Parser_Helper
{
    private $contentArr;

    public function __construct($content)
    {
        $this->contentArr = explode(PHP_EOL, $content);
    }

    public function getValues($directive)
    {
        $result = [];

        $pattern = "/^".$directive.":/i";
        foreach ($this->contentArr as $row) {

            //TODO Is case sensivity?

            if (preg_match($pattern, trim($this->excludeCommens($row)))) {
                $result[] = $this->getValue($row);
            }
        }

        return $result;
    }

    private function excludeCommens($row)
    {
        $commentPos = strpos($row, '#');
        if (!($commentPos === false)) {
            return substr($row, 0, $commentPos - 1);
        } else {
            return $row;
        }
    }

    private function getValue($row)
    {
        $delimiterPos = strpos($row, ':');

        return substr($row, $delimiterPos, strlen($row) - 1);
    }
}