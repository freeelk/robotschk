<?php
namespace app\helpers;

class Robots_Request_Helper
{

    private $useRedirect;
    private $userAgent;

    public function __construct($useRedirect, $userAgent = '') {
        $this->useRedirect = $useRedirect;
        $this->userAgent = $userAgent;
    }

    public function makeRequest($url)
    {
        $result = [];
        $result['url'] = $url;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url."/robots.txt");
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_VERBOSE, true);

        if ($this->useRedirect) {
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        }

        if ($this->userAgent != '') {
            curl_setopt($ch, CURLOPT_USERAGENT, $this->userAgent);
        }

        $response = curl_exec($ch);
        $result['errno'] = curl_errno($ch);
        if (!$result['errno']) {
            $info = curl_getinfo($ch);
            $result['httpCode'] = $info['http_code'];
            $result['httpHeader'] = substr($response, 0, $info['header_size']);
            $result['content'] = substr($response, $info['header_size']);
            $result['fileSize'] = strlen($result['content']);
        }

        curl_close($ch);

        return $result;
    }
}

