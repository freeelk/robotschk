<?php
namespace app\models;

class Model_Checks
{
    const PARAM_PLACE_HOLDER = '<?>';

    private $checksModels;
    private $data;

    public function __construct(array $checksModels, array $data)
    {
        $this->checksModels = $checksModels;
        $this->data = $data;
    }

    public function makeChecks()
    {
        $result = [];

        foreach ($this->checksModels as $model) {
            $row = $model->getRow();
            $result[] = $this->makeCheck($row);
        }

        return $result;
    }

    private function makeCheck(array $row)
    {
        $result = [];

        $checkClassName = '\app\checks\\'.trim($row['chk_name']).'_Check';
        $checkClass = new $checkClassName($this->data);

        $result['number'] = $row['number'];
        $result['name'] = $row['name'];

        try {
            $checkResult = $checkClass->makeCheck();
        } catch (\Exception $e) {
            $checkResult = false;
            $row['state_err'] = 'Проверку не удалось выполнить. '.
                'Ошибка: '.$e->getMessage();
            $row['recommend_err'] = "";

        }

        if ($checkResult) {
            $result['status'] = 'OK';
            $result['state'] = $this->applyParams(
                $row['state_ok'],
                $checkClass->getParams()
            );
            $result['recommend'] = $row['recommend_ok'];
        } else {
            $result['status'] = 'Ошибка';
            $result['state'] = $this->applyParams(
                $row['state_err'],
                $checkClass->getParams()
            );
            $result['recommend'] = $row['recommend_err'];
        }

        return $result;
    }

    private function applyParams($str, $params)
    {
        foreach ($params as $paramValue) {
            $str = $this->strReplaceOnce(
                Self::PARAM_PLACE_HOLDER,
                $paramValue,
                $str
            );
        }

        return $str;

    }

    private function strReplaceOnce($search, $replace, $text)
    {
        $pos = strpos($text, $search);

        return $pos !== false ? substr_replace(
            $text,
            $replace,
            $pos,
            strlen($search)
        ) : $text;
    }
}