<?php namespace app\models\table_models;

class Table_Model
{
    protected $row;

    public function __construct($row)
    {
        $this->row = $row;
    }

    public function getRow()
    {
        return $this->row;
    }

    public static function find(array $fields = ['*'])
    {
        $table = static::table();

        $fieldsStr = implode(', ', $fields);

        $sql = "SELECT $fieldsStr FROM $table;";

        $DBH = \app\core\Database::getInstance()->getConnection();
        try {
            $STH = $DBH->query($sql);
            $STH->setFetchMode(\PDO::FETCH_ASSOC);
            $rows = $STH->fetchAll();
            $models = [];
            $className = static::class;

            foreach ($rows as $index => $row) {
                $models[$index] = new $className($row);
            }

            return $models;

        } catch (\PDOException $e) {
            throw new \Exception(
                "Ошибка базы данных: ".$e->getMessage()."SQL: $sql "
            );
        }
    }

    public static function findOne($id)
    {
        $table = static::table();

        $sql = "SELECT * FROM $table WHERE {$table}.id = :id;";

        $DBH = \app\core\Database::getInstance()->getConnection();
        try {
            $STH = $DBH->prepare($sql);
            $STH->bindParam(':id', $id);
            $STH->execute();
            $STH->setFetchMode(\PDO::FETCH_ASSOC);
            $row = $STH->fetch();
            $className = static::class;

            return new $className($row);
        } catch (PDOException $e) {
            throw new \Exception(
                "Ошибка базы данных: ".$e->getMessage()."SQL: $sql "
            );
        }
    }

    public static function exists($id)
    {
        $table = static::table();

        $sql = "SELECT COUNT('id') AS id FROM $table WHERE {$table}.id = :id;";

        $DBH = \app\core\Database::getInstance()->getConnection();
        try {
            $STH = $DBH->prepare($sql);
            $STH->bindParam(':id', $id);
            $STH->execute();
            $STH->setFetchMode(\PDO::FETCH_ASSOC);
            $row = $STH->fetch();

            return ($row['id'] == 1);

        } catch (PDOException $e) {
            throw new \Exception(
                "Ошибка базы данных: ".$e->getMessage()."SQL: $sql "
            );
        }
    }

    public function save($new = false)
    {
        if ($new) {
            $this->row['id'] = null;
        }

        if (!(is_null($this->row['id']) || ($this->row['id'] < 1))) {
            static::update(['id' => $this->row['id']], $this->row);
            $className = static::class;

            return new $className($this->row);

        } else {
            return static::create($this->row);
        }
    }

    public static function create($row)
    {
        $className = static::class;
        $model = new $className($row);

        $table = static::table();

        foreach ($row as $fieldName => $value) {
            $fieldNames[] = $fieldName;
            $values[] = $value;
        }

        $keys = array_keys($values);
        $fields = implode(', ', $fieldNames);
        $placeHolders = implode(', ', array_fill(0, count($values), '?'));
        $sql = "INSERT INTO $table ($fields) values ($placeHolders);";

        $DBH = \app\core\Database::getInstance()->getConnection();
        try {
            $STH = $DBH->prepare($sql);
            for ($i = 0; $i < count($values); $i++) {
                $STH->bindParam($i + 1, $values[$keys[$i]]);
            }
            $STH->execute();

            $model->id = $DBH->lastInsertId();

            return $model;
        } catch (\PDOException $e) {
            throw new \Exception(
                "Ошибка базы данных: ".$e->getMessage()."SQL: $sql "
            );
        }
    }

    public static function update(array $condition, array $data)
    {
        $table = static::table();
        $conditionTmp = [];
        foreach ($condition as $field => $value) {
            $conditionTmp[] = "$field = '$value'";
        }

        $conditionStr = implode(' AND ', $conditionTmp);

        $dataTmp = [];
        $params = [];
        foreach ($data as $field => $value) {
            $dataTmp[] = "$field = ?";
            $params[] = $value;
        }
        $dataStr = implode(', ', $dataTmp);

        $sql = "UPDATE $table SET $dataStr WHERE $conditionStr;";

        $DBH = \app\core\Database::getInstance()->getConnection();
        try {

            $STH = $DBH->prepare($sql);
            $STH->execute($params);
        } catch (\PDOException $e) {
            throw new \Exception(
                "Ошибка базы данных: ".$e->getMessage()."SQL: $sql "
            );
        }
    }
}