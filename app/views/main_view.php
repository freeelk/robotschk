<div id="input-area" class="">
    <form action="/result/" method="post">
        <div class="input-group">
            <input id="input-url" type="url" pattern="https?://.+" required
                   class="form-control" name="url"
                   placeholder="Введите URL сайта для проверки (http://...)">
                <span class="input-group-btn">
                    <button id="robotsrequest-submit" class="btn btn-default"
                            type="submit">
                        Проверить
                    </button>
                </span>
        </div>
        <div class="checkbox">
            <label>
                <input name="redirect-enable" type="checkbox" checked> Разрешить перенаправление
            </label>
            <label>
                <input name="useragent-enable" type="checkbox" checked> Использовать user agent
            </label>
            <input name="useragent" type="text" class="form-control" value="<?php echo($data['useragent']) ?> ">

        </div>

    </form>
</div>
