<ul class="list-group">
    <li class="list-group-item">Размер
        (байт): <?php echo($data['requestResponse']['fileSize']); ?></li>
    <li class="list-group-item">Код ответа
        сервера: <?php echo($data['requestResponse']['httpCode']); ?></li>
</ul>

<div id="http-headers-area">
    <button class="btn btn-primary" type="button" data-toggle="collapse"
            data-target="#collapseHttpHeader" aria-expanded="false"
            aria-controls="collapseHttpHeader">
        HTTP заголовки
    </button>
    <div class="collapse" id="collapseHttpHeader">
        <div class="well">
            <pre><?php echo($data['requestResponse']['httpHeader']); ?></pre>
        </div>
    </div>
</div>
<div id="robots-content-area">
    <button class="btn btn-primary" type="button" data-toggle="collapse"
            data-target="#collapseContent" aria-expanded="false"
            aria-controls="collapseContent">
        Содержимое ответа сервера
    </button>
    <div class="collapse" id="collapseContent">
        <div class="well">
            <pre><?php echo(htmlspecialchars($data['requestResponse']['content'])); ?></pre>
        </div>
    </div>
</div>