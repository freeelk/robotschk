<div id="input-area">
    <form action="/result/" method="post">
        <div class="input-group">
            <input id="input-url" type="url" pattern="https?://.+" required
                   class="form-control" name="url"
                   placeholder="Введите URL сайта для проверки (http://...)"
                   value=" <?php echo $data['url']; ?>">
                <span class="input-group-btn">
                    <button id="robotsrequest-submit" class="btn btn-default"
                            type="submit">
                        Проверить
                    </button>
                </span>
        </div>
        <div class="checkbox">
            <label>
                <input name="redirect-enable" type="checkbox" <?php echo($data['redirect-enable'] ? 'checked': ''); ?>> Разрешить перенаправление
            </label>
            <label>
                <input name="useragent-enable" type="checkbox" <?php echo($data['useragent-enable'] ? 'checked': ''); ?>> Использовать user agent
            </label>
            <input name="useragent" type="text" class="form-control"
                   value="<?php echo($data['useragent']);?>"  >

        </div>
    </form>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Общая информация о файле <?php echo($data['url']
                .'/robots.txt'); ?></h3>
    </div>

    <?php if ($data['requestResponse']['errno'] == 0) {
        include 'app/views/result_file_exists_view.php';
    } else {
        include 'app/views/result_file_not_exists_view.php';
    } ?>

</div>


<h2>Результат проверок</h2>
<table id="result-table" , class="table table-bordered">
    <tr>
        <th>Номер</th>
        <th>Название проверки</th>
        <th>Статус</th>
        <th></th>
        <th>Текущее состояние</th>
    </tr>
    <?php
    foreach ($data['checksResult'] as $row) {
        $statusStyle = $row['status'] == 'OK' ? 'result-status-ok'
            : 'result-status-err';
        echo "<tr>
              <td rowspan='2'>{$row['number']}</td>
              <td rowspan='2'>{$row['name']}</td>
              <td rowspan='2' class='{$statusStyle}'>{$row['status']}</td>
              <td>Состояние</td>
              <td>{$row['state']}</td>
              </tr>
              <tr>
              <td>Рекомендации</td>
              <td>{$row['recommend']}</td>
              </tr>";


    }
    ?>
</table>
