<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8"/>
    <title>Проверка файла robots.txt</title>
    <link href="/assets/template/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/template/css/main.css" rel="stylesheet">
</head>

<body>

<div class="container wrapper">
    <div class="content">
        <div class="page-header">
            <h1>Проверка файла robots.txt</h1>
        </div>
        <div class="container">
            <?php include 'app/views/'.$content_view; ?>
        </div>
    </div>
    <div class="footer">
        <hr>
        <p>© Владимир Шелест. 2016</p>
    </div>
</div>
<!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script
    src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="/assets/template/js/bootstrap.min.js"></script>
<script src="/assets/template/js/main.js"></script>


</body>

</html>