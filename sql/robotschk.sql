-- phpMyAdmin SQL Dump
-- version 4.6.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Час створення: Чрв 26 2016 р., 21:48
-- Версія сервера: 5.5.49-0ubuntu0.14.04.1
-- Версія PHP: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `robotschk`
--
CREATE DATABASE IF NOT EXISTS `robotschk` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `robotschk`;

-- --------------------------------------------------------

--
-- Структура таблиці `checks`
--

CREATE TABLE `checks` (
  `id` int(11) NOT NULL,
  `chk_name` varchar(20) NOT NULL,
  `number` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `state_ok` text NOT NULL,
  `state_err` text NOT NULL,
  `recommend_ok` text NOT NULL,
  `recommend_err` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `checks`
--

INSERT INTO `checks` (`id`, `chk_name`, `number`, `name`, `state_ok`, `state_err`, `recommend_ok`, `recommend_err`) VALUES
(1, 'Robots_Exists', 1, 'Проверка наличия файла robots.txt', 'Файл robots.txt присутствует', 'Файл robots.txt отсутствует', 'Доработки не требуются', 'Программист: Создать файл robots.txt и разместить его на сайте.'),
(2, 'Host_Exists', 6, 'Проверка указания директивы Host', 'Директива Host указана', 'В файле robots.txt не указана директива Host', 'Доработки не требуются', 'Программист: Для того, чтобы поисковые системы знали, какая версия сайта является основных зеркалом, необходимо прописать адрес основного зеркала в директиве Host. В данный момент это не прописано. Необходимо добавить в файл robots.txt директиву Host. Директива Host задётся в файле 1 раз, после всех правил.'),
(3, 'Host_Count', 8, 'Проверка количества директив Host, прописанных в файле', 'В файле прописана 1 директива Host', 'В файле прописано <?> директив Host', 'Доработки не требуются', 'Программист: Директива Host должна быть указана в файле толоко 1 раз. Необходимо удалить все дополнительные директивы Host и оставить только 1, корректную и соответствующую основному зеркалу сайта'),
(4, 'File_Size', 10, 'Проверка размера файла robots.txt', 'Размер файла robots.txt составляет <?>, что находится в пределах допустимой нормы', 'Размера файла robots.txt составляет <?>, что превышает допустимую норму', 'Доработки не требуются', 'Программист: Максимально допустимый размер файла robots.txt составляем 32 кб. Необходимо отредактировть файл robots.txt таким образом, чтобы его размер не превышал 32 Кб'),
(5, 'Sitemap_Exists', 11, 'Проверка указания директивы Sitemap', 'Директива Sitemap указана', 'В файле robots.txt не указана директива Sitemap', 'Доработки не требуются', 'Программист: Добавить в файл robots.txt директиву Sitemap'),
(6, 'Http_Code', 12, 'Проверка кода ответа сервера для файла robots.txt', 'Файл robots.txt отдаёт код ответа сервера 200', 'При обращении к файлу robots.txt сервер возвращает код ответа <?>', 'Доработки не требуются', 'Программист: Файл robots.txt должны отдавать код ответа 200, иначе файл не будет обрабатываться. Необходимо настроить сайт таким образом, чтобы при обращении к файлу sitemap.xml сервер возвращает код ответа 200');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
